var twilio = require('twilio');
var MongoClient = require('mongodb').MongoClient;
var Server = require('mongodb').Server;
var accountSid = 'AC32a3c49700934481addd5ce1659f04d2';
var authToken = "be967dea0ef735e467a9a8f3906dd5a5";
/**
 * MessageController
 *
 * @module      :: Controller
 * @description	:: A set of functions called `actions`.
 *
 *                 Actions contain code telling Sails how to respond to a certain type of request.
 *                 (i.e. do stuff, then send some JSON, show an HTML page, or redirect to another URL)
 *
 *                 You can configure the blueprint URLs which trigger these actions (`config/controllers.js`)
 *                 and/or override them with custom routes (`config/routes.js`)
 *
 *                 NOTE: The code you write here supports both HTTP and Socket.io automatically.
 *
 * @docs        :: http://sailsjs.org/#!documentation/controllers
 */

var MessageController = {
	received: function(req, res) {
		var query = req.query;
    
	    var request = query.Body.split(" ");
	    
	    var mongoClient = new mongoClient( new Server('170.7.172', 27017), {native_parser: true} );

	    mongoClient.open( function (err, mongoclient) {

	    	if(err) throw err;

	    	var db = mongoClient.db('test');

	    	var command = request[0].toUpperCase();

	    	var response = new twilio.TwimlResponse();

	    	if ( command === "SUBSCRIBE" ) {
	    		db.collection('subscriptions').findAndModify({
	    			'phoneNumber': query.From,
	    			'location-city': query.FromCity,
	    			'location-country': query.FromCountry,
	    			'subscription': request[1]
	    		}, {upsert: true}, function(err, result) {
	    			if err throw err;

	    			mongoClient.close();
	    		});

	    		response.sms("You are now subscribed to " + request[1].toLowerCase() + " prices in your area.");
	    	
	    	} else if ( command === "UPDATE" ) {
	    		db.collection('subscriptions').find({phoneNumber: query.From}, {phoneNumber: 0, location-city: 0, location-country: 0, subscription: 1, _id:0}, function(err, results) {
	    			results.each( function( err, item) {
	    				client.sms.messages.create({
						    body: "Your current ticker price is API GOES HERE",
						    to: query.From,
						    from: "+12674634232"
						}, function(err, message) {
						    process.stdout.write(message);
						});
	    			});
	    		});

	    	}

	    	res.writeHead(200, function() {
	    		'Content-Type':'text/xml'
	    	});

	    	res.end(response.toString());
	    	console.log(response.toString());

	    }
	    
	}
}

module.exports = {
    
  


  /**
   * Overrides for the settings in `config/controllers.js`
   * (specific to MessageController)
   */
  _config: {}

  
};
